#!/usr/bin/env python
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-incomedeclaration@zougloub.eu> & contributors
# SPDX-License-Identifier: AGPL-3.0-only

import typing as t
import logging
import sys
import os
import tempfile
import urllib.request
import xml.etree.ElementTree as ET
import hashlib


logger = logging.getLogger(__name__)


user_agent = os.environ.get("MAPLETAX_USER_AGENT", 'Mozilla/5.0 ' \
 '(Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 ' \
 '(KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36')


def download(file_name, url, hash_obj, reference_digest=None):
	"""
	Download to `file_name` from `url` updating `hash_obj`
	and comparing digest to `reference_digest` if supplied.
	"""

	temp_file_name = f"{file_name}.part"
	if os.path.exists(file_name):
		os.unlink(file_name)

	headers = {
	 'User-Agent': user_agent,
	}
	req = urllib.request.Request(
	 url,
	 data=None,
	 headers=headers,
	)
	with (
	  urllib.request.urlopen(req) as response,
	  open(temp_file_name, "wb") as temp_file,
	 ):
		while chunk := response.read(8192):
			temp_file.write(chunk)
			hash_obj.update(chunk)

	if reference_digest:
		calculated_checksum = \
		 hash_obj.hexdigest() if isinstance(reference_digest, str) else hash_obj.digest()

		if calculated_checksum != reference_digest:
			raise ValueError(f"Checksum verification failed for {file_name}")

	os.rename(temp_file_name, file_name)


def create_metalink(path, url):
	"""
	Create metalink file for local download file `path`,
	for URL `url`.
	"""

	hash_obj = hashlib.sha256()
	download(path, url, hash_obj, None)
	hash_hex = hash_obj.hexdigest()

	metalink = ET.Element('metalink', xmlns='urn:ietf:params:xml:ns:metalink')
	file_element = ET.SubElement(metalink, 'file', name=os.path.basename(path))
	ET.SubElement(file_element, 'url').text = url
	ET.SubElement(file_element, 'hash', type='sha-256').text = hash_hex
	tree = ET.ElementTree(metalink)
	metalink_file_name = os.path.splitext(path)[0] + '.meta4'
	tree.write(metalink_file_name, encoding='utf-8', xml_declaration=True)


def download_metalink(path) -> t.Mapping[str,t.Optional[Exception]]:
	"""
	Download files given a metalink file.
	The files are downloaded to cwd.

	:return: dictionary containing paths as keys and optional error
	 as value
	"""

	tree = ET.parse(path)
	root = tree.getroot()
	ns = {'ml': 'urn:ietf:params:xml:ns:metalink'}

	out = dict()

	for file_element in root.findall(".//ml:file", ns):
		file_name = file_element.get("name")
		file_url = file_element.find("ml:url", ns).text
		checksum_value = file_element.find("ml:hash", ns).text
		checksum_type = file_element.find("ml:hash", ns).get("type")

		if checksum_type.lower() == "sha-256":
			hash_obj = hashlib.sha256()
		else:
			out[file_name] = NotImplemented(f"Unsupported checksum type: {checksum_type}")

		try:
			download(file_name, file_url, hash_obj)
		except Exception as e:
			out[file_name] = e
		else:
			out[file_name] = None

	return out


def main(argv=None):
	import argparse
	parser = argparse.ArgumentParser(
	 description="Income declaration forms tool",
	)

	parser.add_argument("--log-level",
	 default="WARNING",
	 help="Logging level (eg. INFO, see Python logging docs)",
	)

	subparsers = parser.add_subparsers(
	 help='the command; type "%s COMMAND -h" for command-specific help' % sys.argv[0],
	 dest='command',
	)


	subp = subparsers.add_parser(
	 "add",
	 help="Add a form",
	)

	subp.add_argument("path",
	)

	subp.add_argument("url",
	)

	def doit(args):
		create_metalink(args.path, args.url)

	subp.set_defaults(func=doit)


	args = parser.parse_args(argv)


	try:
		import coloredlogs
		coloredlogs.install(
		 level=getattr(logging, args.log_level),
		 logger=logging.root,
		 isatty=True,
		)
	except ImportError:
		logging.basicConfig(
		 datefmt="%Y%m%dT%H%M%S",
		 level=getattr(logging, args.log_level),
		 format="%(asctime)-15s %(name)s %(levelname)s %(message)s"
		)

	func = getattr(args, 'func', None)
	if func is not None:
		args.func(args)
	else:
		parser.print_help()
		return 1


if __name__ == "__main__":
	raise SystemExit(main())
