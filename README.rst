#################################
Canadian Income Declaration Forms
#################################


This repository contains a copy of canadian income declaration forms,
and corresponding metadata (metalink files, giving the file download
link and file checksum, for verification purposes).


The files are stored by year.


Sources
#######

Federal
*******

The files have been downloaded from:
https://www.canada.ca/en/revenue-agency/services/forms-publications/tax-packages-years/general-income-tax-benefit-package.html


Québec
******

The files have been downloaded from the links provided on the Revenu Québec website:
https://www.revenuquebec.ca/fr/services-en-ligne/formulaires-et-publications/details-courant/tp-1/


Verification
############

To verify the authenticity of the copies of the government forms that
lie in this repository:

- Review metalink files ; they are on a single line and contain a
  single URL, which is either under https://www.revenuquebec.ca or
  https://www.canada.ca.

- Go in a temporary directory
- Download files, eg. using `aria2c /path/to/*.meta4`
- Compare files in your directory vs. those in the repository
