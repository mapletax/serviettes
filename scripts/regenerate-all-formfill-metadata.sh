#!/bin/sh
# Regenerate form-filling metadata from PDFs

for year in 22 23; do
	for lang in e f; do
		for file in 20${year}/*-fill-${year}${lang}.pdf; do
			${LAUNCHER} python -m mapletax.poutine --log-level INFO federal "${file//.pdf/.json}" "$file" $(basename ${file//-fill-${year}${lang}.pdf/}) 20${year}
		done
	done
done
for year in 21 22 23; do
	for file in 20${year}/TP-1.D?????-???.pdf; do
		${LAUNCHER} python -m mapletax.poutine --log-level INFO qc "${file//.pdf/.json}" "$file" TP-1.D 20${year}
		if [ "$?" = "0" ]; then
			:
		else
			break
		fi
	done
	for annex in A B C D E F G H I J K L M N O P Q R S T U V W X Y Z; do
		for file in 20${year}/TP-1.D.${annex}?????-???.pdf; do
			if [ -f "${file}" ]; then
				case "$file" in
					2021/TP-1.D.J*|2022/TP-1.D.J*)
						: # Too slow for now
					;;
					*)
						${LAUNCHER} python -m mapletax.poutine --log-level INFO qc "${file//.pdf/.json}" "$file" TP-1.D.${annex} 20${year}
					;;
				esac
			fi
		done
	done
done
